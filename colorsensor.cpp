#include <colorsensor.h>

// ATM all these functions blow up if you pass NoColor
// Returns a CdS object for given color
CdS ColorSensor::getCdS(int num) { return num == 1 ? CdS1 : CdS2; }

// Getter for maxs of color ranges
const float ColorSensor::getMax(Color color) { return color == Red ? RED_MAX : BLUE_MAX; }

// Getter for mins of color ranges
const float ColorSensor::getMin(Color color) { return color == Red ? RED_MIN : BLUE_MIN; }

// Checks if certain color light is present
bool ColorSensor::isColor(Color color)
{
    float value = getCdS(2).Value();
    return value > getMin(color) && value < getMax(color);
}

// Returns what color (if any) is present
ColorSensor::Color ColorSensor::getColor(void)
{
    return isColor(Red) ? Red : isColor(Blue) ? Blue : NoColor;
}

// First thing to run at beginning of course
void ColorSensor::waitForStart(void)
{
    LCD.Clear();
    float startTime = TimeNowSec();
    float elapsed = TimeNowSec() - startTime;
    while(!isColor(Red) && elapsed < 30)
    {
        elapsed = TimeNowSec() - startTime;
        if (30 - elapsed < 10)
        {
            LCD.WriteRC(0, 0, 0);
            LCD.WriteRC(30 - int(elapsed), 0, 1);
        }
        else
            LCD.WriteRC(30 - int(elapsed), 0, 0);
    }
    LCD.Clear();
}

// Returns CdS's raw value for debugging purposes
float ColorSensor::getRaw(int num)
{
    return getCdS(num).Value();
}

#include <tests.h>

// This function is so damn long I gave it
// its own file
void Tests::testMenu(void)
{
    bool chose;
    char choices[] = "ABCDEFGH";
    int index = 0, pressed, len = std::strlen(choices) - 1;
    while (true)
    {
        LCD.Clear();
        bT.setAngle(UP_ANGLE);
        nA.raise(false);
        chose = false;
        while (!chose)
        {
            //LCD.Clear();
            LCD.WriteRC("A) Drivetrain", 0, 0);
            LCD.WriteRC("B) Color Sensor", 1, 0);
            LCD.WriteRC("C) Other", 2, 0);
            LCD.WriteRC("D) Crank", 3, 0);
            LCD.WriteRC("E) Switch", 4, 0);
            LCD.WriteRC("F) Salt", 5, 0);
            LCD.WriteRC("G) Buttons", 6, 0);
            LCD.WriteRC("H) Performance Tests", 7, 0);
            LCD.WriteRC("Choice: ", 9, 0);
            LCD.WriteRC(choices[index], 9, 8);
            pressed = getPressed();
            switch (pressed)
            {
                case 0:
                    index--;
                    break;
                case 1:
                    chose = true;
                    break;
                case 2:
                    index++;
                    break;
            }
            index = index < 0 ? len : index > len ? 0 : index;
        }
        chose = false;
        int index2 = 0;
        char choices2[] = "ABCDEF";
        int len2 = std::strlen(choices2) - 1;
        LCD.Clear();
        while (!chose)
        {
            switch (index)
            {
                // Drivetrain
                case 0:
                    LCD.WriteRC("A) Encoder and RPS", 0, 0);
                    LCD.WriteRC("B) Run Motors", 1, 0);
                    LCD.WriteRC("C) Print Encoders", 2, 0);
                    LCD.WriteRC("D) Test Linear Correct", 3, 0);
                    LCD.WriteRC("E) Test Angular Correct", 4, 0);
                    LCD.WriteRC("F) Test Pulse", 5, 0);
                    break;
                // Color Sensor
                case 1:
                    LCD.WriteRC("A) Test Sensor", 0, 0);
                    LCD.WriteRC("B) Nothing", 1, 0);
                    LCD.WriteRC("C) Nothing", 2, 0);
                    LCD.WriteRC("D) Nothing", 3, 0);
                    LCD.WriteRC("E) Nothing", 4, 0);
                    LCD.WriteRC("F) Nothing", 5, 0);
                    break;
                // Other
                case 2:
                    LCD.WriteRC("A) Print RPS", 0, 0);
                    LCD.WriteRC("B) Print Battery", 1, 0);
                    LCD.WriteRC("C) Toggle Debug", 2, 0);
                    LCD.WriteRC("D) Nothing", 3, 0);
                    LCD.WriteRC("E) Nothing", 4, 0);
                    LCD.WriteRC("F) Nothing", 5, 0);
                    break;
                // Crank
                case 3:
                    LCD.WriteRC("A) Test Crank", 0, 0);
                    LCD.WriteRC("B) Run Crank Full", 1, 0);
                    LCD.WriteRC("C) Nothing", 2, 0);
                    LCD.WriteRC("D) Nothing", 3, 0);
                    LCD.WriteRC("E) Nothing", 4, 0);
                    LCD.WriteRC("F) Nothing", 5, 0);
                    break;
                // Switch
                case 4:
                    LCD.WriteRC("A) Run", 0, 0);
                    LCD.WriteRC("B) Nothing", 1, 0);
                    LCD.WriteRC("C) Nothing", 2, 0);
                    LCD.WriteRC("D) Nothing", 3, 0);
                    LCD.WriteRC("E) Nothing", 4, 0);
                    LCD.WriteRC("F) Nothing", 5, 0);
                    break;
                // Salt
                case 5:
                    LCD.WriteRC("A) Calibrate", 0, 0);
                    LCD.WriteRC("B) Print Angle", 1, 0);
                    LCD.WriteRC("C) Deposit", 2, 0);
                    LCD.WriteRC("D) Print Switch", 3, 0);
                    LCD.WriteRC("E) Nothing", 4, 0);
                    LCD.WriteRC("F) Nothing", 5, 0);
                    break;
                // Buttons
                case 6:
                    LCD.WriteRC("A) Calibrate", 0, 0);
                    LCD.WriteRC("B) Print Angle", 1, 0);
                    LCD.WriteRC("C) Run Shit", 2, 0);
                    LCD.WriteRC("D) Run Section", 3, 0);
                    LCD.WriteRC("E) Nothing", 4, 0);
                    LCD.WriteRC("F) Nothing", 5, 0);
                    break;
                // PT's
                case 7:
                    LCD.WriteRC("A) PT2", 0, 0);
                    LCD.WriteRC("B) PT3", 1, 0);
                    LCD.WriteRC("C) PT4", 2, 0);
                    LCD.WriteRC("D) PT5", 3, 0);
                    LCD.WriteRC("E) Main", 4, 0);
                    LCD.WriteRC("F) Nothing", 5, 0);
                    break;
            }
            LCD.WriteRC("Choice: ", 7, 0);
            LCD.WriteRC(choices2[index2], 7, 8);
            pressed = getPressed();
            switch (pressed)
            {
                case 0:
                    index2--;
                    break;
                case 1:
                    chose = true;
                    break;
                case 2:
                    index2++;
                    break;
            }
            index2 = index2 < 0 ? len2 : index2 > len2 ? 0 : index2;
        }
        LCD.Clear();
        switch (index)
        {
            // Drivetrain
            case 0:
                switch (index2)
                {
                    case 0:
                        testEncAndRPS();
                        break;
                    case 1:
                        testMotors();
                        break;
                    case 2:
                        testEnc();
                        break;
                    case 3:
                        testLinearCorrect();
                        break;
                    case 4:
                        testAngularCorrect();
                        break;
                    case 5:
                        testPulse();
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                }
                break;
            // Color Sensor
            case 1:
                switch (index2)
                {
                    case 0:
                        testColors();
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                }
                break;
            // Other
            case 2:
                switch (index2)
                {
                    case 0:
                        printRPS();
                        break;
                    case 1:
                        printBattery();
                        break;
                    case 2:
                        toggleDebug();
                        LCD.Clear();
                        LCD.Write("Debug is now ");
                        LCD.WriteLine(getDebug());
                        Sleep(1000);
                        LCD.Clear();
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                }
                break;
            // Crank
            case 3:
                switch (index2)
                {
                    case 0:
                        runCrank();
                        break;
                    case 1:
                        dT.reverseOrientation();
                        cR.run();
                        dT.reverseOrientation();
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                }
                break;
            // Switch
            case 4:
                switch (index2)
                {
                    case 0:
                        dT.reverseOrientation();
                        sW.run();
                        dT.reverseOrientation();
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                }
                break;
            // Salt
            case 5:
                switch (index2)
                {
                    case 0:
                        calibrateSalt();
                        break;
                    case 1:
                        printSaltAngle();
                        break;
                    case 2:
                        nA.deposit();
                        break;
                    case 3:
                        while(!middlePressed())
                        {
                            LCD.Clear();
                            LCD.WriteLine(nA.getSwitch());
                            Sleep(200);
                        }
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                }
                break;
            // Buttons
            case 6:
                switch (index2)
                {
                    case 0:
                        calibrateServo();
                        break;
                    case 1:
                        printAngle();
                        break;
                    case 2:
                        runShit();
                        break;
                    case 3:
                        dT.reverseOrientation();
                        bT.run();
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                }
                break;
            // PT's
            case 7:
                switch (index2)
                {
                    case 0:
                        pT2();
                        break;
                    case 1:
                        pT3();
                        break;
                    case 2:
                        pT4();
                        break;
                    case 3:
                        pT5();
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                }
                break;
        }
    }
}

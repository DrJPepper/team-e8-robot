#include <FEHIO.h>
#include <globals.h>

#ifndef COLORSENSOR_H
#define COLORSENSOR_H

class ColorSensor
{
    public:
        // CONSTRUCTOR
        ColorSensor(void): CdS1(CDS_1), CdS2(CDS_2){}
        // OBJECTS
        enum Color {Red, Blue, NoColor};
        // METHODS
        bool isColor(Color);
        Color getColor(void);
        void waitForStart(void);
        float getRaw(int);
    private:
        // OBJECTS
        CdS CdS1;
        CdS CdS2;
        // METHODS
        CdS getCdS(int);
        const float getMax(Color);
        const float getMin(Color);
};

#endif // COLORSENSOR_H

#include <FEHLCD.h>
#include <FEHRPS.h>
#include <FEHUtility.h>
#include <drivetrain.h>
#include <colorsensor.h>
#include <buttons.h>
#include <crank.h>
#include <salt.h>
#include <switch.h>
#include <tests.h>

void run(void);

int main(void)
{

    LCD.Clear( FEHLCD::Black );
    LCD.SetFontColor( FEHLCD::Green );

    //RPS.InitializeMenu();

    Drivetrain dT = Drivetrain();
    ColorSensor cS = ColorSensor();
    Buttons bT = Buttons(dT);
    Crank cR = Crank(cS, dT);
    Salt nA = Salt(dT);
    Switch sW = Switch(dT, bT);

    Tests tests = Tests(dT, cS, bT, nA, cR, sW);

    LCD.WriteLine(" Press middle button for\nmain, right tests w/ RPS, left for tests.");
    switch (getPressed())
    {
        case 2:
            RPS.InitializeMenu();
        case 0:
            tests.testMenu();
            break;
        case 1:
            RPS.InitializeMenu();
            LCD.Clear();
            LCD.WriteLine("Debug (L/M, No | R, Y)?");
            if (getPressed() == 2) toggleDebug();
            LCD.WriteLine("Press R/L to calibrate");
            int c = getPressed();
            if (c == 0 || c == 2) dT.setCorrect();
    }
    nA.retract();
    cS.waitForStart();
    nA.raise();
    Sleep(300);
    dT.driveTo(18, 18.7); // Forward a bit
    //dT.driveToWithHeading(SALT_X, SALT_Y, 315); // To salt
    //nA.run();
    int count = 0;
    while (!nA.bagIn() && count < 4)
    {
        dT.driveToWithHeading(SALT_X, SALT_Y, 315); // To salt
        nA.run();
        count++;
    }
    dT.reverseOrientation(); // Heading for crank
    dT.driveToWithHeading(28.2, 23.5, 95); // Drive up to ramp and kind of line up
    dT.driveToWithSpeed(29.0, 41.6, DRIVING_SPEED + 0.2); // Up ramp
    cR.run();
    bT.run();
    nA.deposit();
    sW.run();
    LCD.WriteLine("ayy lmao");

    return 0;
}


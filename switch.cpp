#include "switch.h"
#include <FEHRPS.h>

void Switch::run(void)
{
    dT.driveTo(7.9, 46.5); // Move over to avoid shed thing
    dT.driveTo(6.0, 41.0); // Top of Ramp
    dT.driveTo(8.6, 25.3); // Bottom of Ramp
    switch (RPS.OilDirec())
    {
    case 0: // Right
        debugPrint("Right\n");
        dT.driveTo(17.2, 20.6); // Side of switch
        dT.driveToWithHeading(16.0, 11.5, 270); // In front of switch
        bT.setAngle(54); // Lower
        dT.turn(Right, float(TURNING_SPEED + 0.2));
        Sleep(2000);
        dT.drive(0.0);
        while (!RPS.OilPress())
        {
            dT.reverseOrientation();
            dT.driveTo(15.3, 21.0); // Side of switch
            dT.reverseOrientation();
            dT.driveToWithHeading(16.0, 11.5, 270); // In front of switch
            dT.turn(Right, float(TURNING_SPEED + 0.2));
            Sleep(2000);
            dT.drive(0.0);
        }
        break;
    default: // Left
        dT.driveToWithHeading(11.4, 21.8, 270); // Line up to ram
        bT.setAngle(45); // Lower
        Sleep(1000);
        dT.drive(dT.getDrivingSpeed() + 0.1);
        Sleep(2000);
        dT.drive(0.0);
        while (!RPS.OilPress())
        {
            dT.reverseOrientation();
            dT.driveToWithHeading(11.6, 21.8, 90); // Line up to ram
            dT.reverseOrientation();
            dT.drive(dT.getDrivingSpeed() + 0.1);
            Sleep(2000);
            dT.drive(0.0);
        }
    }
}

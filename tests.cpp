#include <tests.h>

void Tests::testEncAndRPS(void)
{
    int button = 3;
    float speed = 0.0;
    while (button != 1)
    {
        LCD.Clear();
        LCD.WriteLine("Press L and R to change speed");
        LCD.WriteLine("Press M to start");
        LCD.Write("Speed: ");
        LCD.WriteLine(speed);
        button = getPressed();
        if (button == 0) speed -= 0.05;
        else if (button == 2) speed += 0.05;
        Sleep(50);
    }
    float x = RPS.X();
    float y = RPS.Y();
    dT.resetEncoders();
    dT.drive(speed);
    Sleep(3000);
    dT.drive(0.0);
    float dist = distance(x, y, RPS.X(), RPS.Y());
    LCD.Write("Distance: ");
    LCD.WriteLine(dist);
    LCD.Write("Clicks: ");
    LCD.WriteLine(dT.clicks());
    LCD.Write("Clicks Per: ");
    LCD.WriteLine(dT.clicks() / dist);
    waitForMiddlePress();
}

// NOT DONE
void Tests::testRotationAndRPS(void)
{
    int button = 3;
    float speed = 0.0;
    while (button != 1)
    {
        button = getPressed();
        if (button == 0) speed -= 0.05;
        else if (button == 2) speed += 0.05;
    }

}

void Tests::testEnc(void)
{
    while (true)
    {
        Sleep(100);
        LCD.Clear();
        LCD.Write("Right: ");
        LCD.WriteLine(dT.clicks(Right));
        LCD.Write("Left: ");
        LCD.WriteLine(dT.clicks(Left));
    }
}

void Tests::printRPS(void)
{
    Sleep(200);
    while (!middlePressed())
    {
        LCD.Clear();
        LCD.Write("X: ");
        LCD.WriteLine(RPS.X());
        LCD.Write("Y: ");
        LCD.WriteLine(RPS.Y());
        LCD.Write("Heading: ");
        LCD.WriteLine(RPS.Heading());
        Sleep(100);
    }
}

void Tests::testMotors(void)
{
    Sleep(100);
    float speed = 0.0;
    while (true)
    {
        LCD.Clear();
        LCD.WriteLine("Press Middle To Quit");
        LCD.Write("Speed (%): ");
        LCD.WriteLine(speed * 100);
        dT.drive(speed);
        switch (getPressed())
        {
            case 0:
                speed -= 0.05;
                break;
            case 1:
                dT.drive(0.0);
                return;
            case 2:
                speed += 0.05;
                break;
        }
        Sleep(100);
    }
}

void Tests::testLinearCorrect(void)
{
    float y = RPS.Y() + 5.0;
    dT.driveTo(y, RPS.Y());
}

void Tests::testAngularCorrect(void)
{
    dT.rotate(90.0);
    while (!middlePressed()){};
}

void Tests::testPulse(void)
{
    for (int i = 0; i < 25; i++)
    {
        dT.turn(Right, true);
    }
}

void Tests::testColors(void)
{
    while (!middlePressed())
    {
        LCD.Clear();
        LCD.Write("Red: ");
        LCD.WriteLine(cS.isColor(cS.Red));
        LCD.Write("Blue: ");
        LCD.WriteLine(cS.isColor(cS.Blue));
        LCD.WriteLine("");
        LCD.Write("Raw Value (1): ");
        LCD.WriteLine(cS.getRaw(1));
        LCD.Write("Raw Value (2): ");
        LCD.WriteLine(cS.getRaw(2));
        Sleep(100);
    }
}

void Tests::calibrateServo(void)
{
    bT.calibrate();
}

void Tests::printAngle(void)
{
    int angle = 90;
    int choice;
    while (true)
    {
        LCD.Clear();
        LCD.Write("Angle: ");
        LCD.WriteLine(angle);
        bT.setAngle(angle);
        choice = getPressed();
        switch(choice)
        {
            case 0:
                angle -= 2;
                break;
            case 1:
                return;
            case 2:
                angle += 2;
        }
    }
}

void Tests::runShit(void)
{
    bT.run();
}

void Tests::printBattery(void)
{
    while(!middlePressed())
    {
        LCD.WriteLine(Battery.Voltage());
        Sleep(INSTRUCTION_DELAY);
        LCD.Clear();
    }
}

void Tests::calibrateSalt(void)
{
    nA.calibrate();
}

void Tests::printSaltAngle(void)
{
    int angle = 90;
    int choice;
    while (true)
    {
        LCD.Clear();
        LCD.Write("Angle: ");
        LCD.WriteLine(angle);
        nA.setAngle(angle);
        choice = getPressed();
        switch(choice)
        {
            case 0:
                angle -= 2;
                break;
            case 1:
                return;
            case 2:
                angle += 2;
        }
    }
}

void Tests::runCrank(void)
{
    LCD.Clear();
    LCD.WriteLine("Press middle to start");
    waitForMiddlePress();
    cR.runMotor(Right);
    float startTime = TimeNow();
    waitForMiddlePress();
    cR.stopMotor();
    float runTime = TimeNow() - startTime;
    LCD.Write("Ran for ");
    LCD.Write(runTime * 1000);
    LCD.WriteLine(" mSeconds");
    waitForMiddlePress();
}

#include <drivetrain.h>

// Resets clicks to 0 for both encoders
void Drivetrain::resetEncoders(void)
{
    debugPrint("Reset Encoders\n");
    right_encoder.ResetCounts();
    left_encoder.ResetCounts();
}

// Just sets the motors to a given speed
void Drivetrain::drive(float magnitude)
{
    debugPrint("Set Motors to: ");
    debugPrint(magnitude);
    right_motor.SetPercent(magnitude * 100);
    left_motor.SetPercent(magnitude * 100);
}

// Optionally just pulses instead of fully sets
void Drivetrain::drive(float magnitude, bool pulse)
{
    drive(magnitude);
    if (pulse)
    {
        Sleep(PULSE_DELAY);
        drive(float(0.0));
        Sleep(PULSE_DELAY);
    }
}

// Uses global pulse speed
void Drivetrain::drive(ForBack dir, bool pulse)
{
    if (pulse)
    {
        if (dir == Forwards)
        {
            debugPrint("Pulsing Forwards\n");
            drive(getPulseSpeed());
        }
        else
        {
            debugPrint("Pulsing Backwards\n");
            drive(-getPulseSpeed());
        }
        Sleep(PULSE_DELAY);
        drive(float(0.0));
        Sleep(PULSE_DELAY);
    }
}

// Just sets motors opposite each other
void Drivetrain::turn(Direction dir, float magnitude)
{
    debugPrint("Turning at: ");
    debugPrint(magnitude);
    float multiplier = dir == Right ? -100 : 100;
    right_motor.SetPercent(magnitude * multiplier);
    left_motor.SetPercent(magnitude * -multiplier);
}

// Optionally just pulses instead of fully sets
void Drivetrain::turn(Direction dir, float magnitude, bool pulse)
{
    turn(dir, magnitude);
    if (pulse)
    {
        Sleep(PULSE_DELAY);
        drive(float(0.0));
        Sleep(PULSE_DELAY);
    }
}

// Uses global pulse speed
void Drivetrain::turn(Direction dir, bool pulse)
{
    turn(dir, PULSE_SPEED, pulse);
}

float Drivetrain::calcAngularError(float target)
{
    float current = getHeading();
    debugPrint("Current: ");
    debugPrint(current, true);
    float err = target - current;
    debugPrint("Uncorrected Error: ");
    debugPrint(err, true);
    err = err >= 180 ? err - 360 : err < -180 ? err + 360 : err;
    debugPrint("Corrected Error: ");
    debugPrint(err, true);
    return err;
}

// Rotates robot given number of degrees based on encoder clicks,
// and optionally corrects via RPS
void Drivetrain::rotate(float theta, bool correct)
{
    float initDir = getHeading();
    // Direction of vector
    float direction = theta - initDir;
    debugPrint("Direction: ");
    debugPrint(direction, true);
    if (fabsf(direction) < ANGULAR_DEADBAND) return;
    debugPrint("Rotating\n", true);

    // Correct for edge case
    if (direction > 180) direction -= 360;
    else if (direction < -180) direction += 360;
    debugPrint("Corrected Direction: ");
    debugPrint(direction, true);
    debugPrint("Init Heading: ");
    debugPrint(initDir, true);
    int angClicks = abs(direction * CLICKS_PER_DEGREE);
    debugPrint("Angular Clicks: ");
    debugPrint(angClicks, true);
    resetEncoders();
    Direction dir;
    if (direction > 0)
    {
        debugPrint("Turning Left\n");
        dir = Right;
    }
    else
    {
        debugPrint("Turning Right\n");
        dir = Left;
    }
    turn(dir, getTurningSpeed());
    int sTime = TimeNowMSec();
    float oldHeading = RPS.Heading();
    while (clicks() < angClicks)
    {
        if ((TimeNowMSec() - sTime) / 1000 > 1)
        {
            if ((fabs(RPS.Heading() - oldHeading) < 0.5))
            {
                turn(dir == Right ? Left : Right, getTurningSpeed());
                Sleep(500);
                rotate(theta, correct);
                return;
            }
            else
            {
                oldHeading = RPS.Heading();
                sTime = TimeNowMSec();
            }
        }
    }
    drive(float(0.0));
    debugPrint("Done Rotating\n");
    if (correct)
    {
        debugPrint("Now Correcting\n");
        float target = initDir + direction;
        debugPrint("Target Angle Uncorrected: ");
        debugPrint(target, true);
        target = target >= 360 ? target - 360 : target < 0 ? target + 360 : target;
        debugPrint("Target Angle Corrected: ");
        debugPrint(target, true);
        Sleep(INSTRUCTION_DELAY);
        float err = calcAngularError(target);
        int count = 0;
        while ((err > ANGULAR_DEADBAND || err < -ANGULAR_DEADBAND) && count < 20)
        {
            if (err > ANGULAR_DEADBAND)
            {
                debugPrint("Pulsing Left\n", true);
                turn(Left, true);
            }
            else if (err < -ANGULAR_DEADBAND)
            {
                debugPrint("Pulsing Right\n", true);
                turn(Right, true);
            }
            err = calcAngularError(target);
            count++;
        }
        debugPrint("Done Correcting Left\n");
    }
}

// Defaults correcting to true
void Drivetrain::rotate(float degrees)
{
    rotate(degrees, true);
}

// Drives to a given point on the course
void Drivetrain::driveTo(float x, float y, float speed, bool correct, float expHeading)
{
    // Correct
    x += corrX;
    y += corrY;
    expHeading += corrHead;

    // Initial values
    float initX = RPS.X();
    float initY = RPS.Y();

    // Deltas
    float dX = x - initX;
    float dY = y - initY;

    // Distance formula
    float magnitude = distance(initX, initY, x, y);
    debugPrint("Magnitude: ");
    debugPrint(magnitude, true);

    // Desired angle of vector relative to horizontal
    float theta = atan(fabsf(dY / dX)) * 180 / PI;
    debugPrint("Theta: ");
    debugPrint(theta, true);

    // Correct for angle relative to cartesian plane
    // II
    if (dY >= 0 && dX < 0)
    {
        debugPrint("Quadrant II\n", true);
        theta = 180 - theta;
    }
    // III
    else if (dY < 0 && dX <= 0)
    {
        debugPrint("Quadrant III\n", true);
        theta += 180;
    }
    // IV
    else if (dY <= 0 && dX > 0)
    {
        debugPrint("Quadrant IV\n", true);
        theta = 360 - theta;
    }
    // I is just theta
    else
    {
        debugPrint("Quadrant I\n", true);
    }
    debugPrint("Corrected Theta: ");
    debugPrint(theta, true);


    // Convert magnitude to encoder clicks
    int linClicks = round(magnitude * CLICKS_PER_INCH);
    debugPrint("Linear Clicks: ");
    debugPrint(linClicks, true);

    // Rotate to drive in straight line
    rotate(theta, correct);

    resetEncoders();
    debugPrint("Driving\n", true);
    drive(speed);

    // Wait to hit click limit
    int sTime = TimeNowMSec();
    float oldX = RPS.X();
    float oldY = RPS.Y();
    while (clicks() < linClicks)
    {
        if ((TimeNowMSec() - sTime) / 1000 > 1)
        {
            if ((fabs(RPS.X() - oldX) < 0.2) && (fabs(RPS.Y() - oldY) < 0.2))
            {
                drive(-speed);
                Sleep(500);
                driveTo(x, y, speed, correct, expHeading);
                return;
            }
            else
            {
                oldX = RPS.X();
                oldY = RPS.Y();
                sTime = TimeNowMSec();
            }
        }
    }
    // Stop
    drive(0.0);
    debugPrint("Stopped\n", true);
    // Wait to settle
    Sleep(INSTRUCTION_DELAY);
    // Turn to heading if specified
    if (expHeading >= 0.0)
    {
        debugPrint("Correcting Rotation\n");
        rotate(expHeading, correct);
    }
    // Corrections done relative to 45 deg shifted cartesian plane,
    // where quadrants II and IV use x and I and III use y
    if (correct)
    {
        // if in II or IV
        debugPrint("Correcting\n");
        int runCount = 0;
        if ((getHeading() >= 315 || getHeading() < 45)
               || (getHeading() >= 135 && getHeading() < 225))
        {
            // Might look funny, but it should prevent robot from dancing
            // back and forth if it over-over corrects (at that point I
            // think it's better to just deal with it versus waste more time)
            debugPrint("In Quadrant II or IV\n", true);
            while ((RPS.X() > x + LINEAR_DEADBAND || RPS.X() < x - LINEAR_DEADBAND) && runCount <= 5)
            {
                if (RPS.X() > x)
                {
                    debugPrint("Correcting Backwards\n", true);
                    drive(Backwards, true);
                }
                else if (RPS.X() < x)
                {
                    debugPrint("Correcting Forwards\n", true);
                    drive(Forwards, true);
                }
                runCount++;
            }
            debugPrint("Not in loop\n");
        }
        else
        {
            debugPrint("In Quadrant I or III\n", true);
            while ((RPS.Y() > y + LINEAR_DEADBAND || RPS.Y() < y - LINEAR_DEADBAND) && runCount <= 5)
            {
                if (RPS.Y() > y)
                {
                    debugPrint("Correcting Backwards\n", true);
                    drive(Backwards, true);
                }
                if (RPS.Y() < y)
                {
                    debugPrint("Correcting Forwards\n", true);
                    drive(Forwards, true);
                }
                runCount++;
            }
        }
    }
}

void Drivetrain::driveTo(float x, float y)
{
    driveTo(x, y, getDrivingSpeed(), true, -1.0);
}

void Drivetrain::driveTo(float x, float y, bool correct)
{
    driveTo(x, y, getDrivingSpeed(), correct, -1.0);
}

void Drivetrain::driveToWithSpeed(float x, float y, float speed)
{
    driveTo(x, y, getOrientation() == Forwards ? speed : -speed, true, -1.0);
}

void Drivetrain::driveToWithSpeed(float x, float y, float speed, bool correct)
{
    driveTo(x, y, getOrientation() == Forwards ? speed : -speed, correct, -1.0);
}

void Drivetrain::driveToWithHeading(float x, float y, float expDirection)
{
    driveTo(x, y, getDrivingSpeed(), true, expDirection);
}

void Drivetrain::driveToWithHeading(float x, float y, float expDirection, bool correct)
{
    driveTo(x, y, getDrivingSpeed(), correct, expDirection);
}

// Returns either right or left side clicks
int Drivetrain::clicks(Direction dir)
{
    return dir == Right ? right_encoder.Counts() : left_encoder.Counts();
}

// Returns average clicks
int Drivetrain::clicks(void)
{
    return int((right_encoder.Counts() + left_encoder.Counts()) / 2.0);
}

float Drivetrain::getTurningSpeed(void)
{
    return turningSpeed;
}

float Drivetrain::getDrivingSpeed(void)
{
    debugPrint("Driving Speed (from getter): ");
    debugPrint(drivingSpeed);
    return drivingSpeed;
}

float Drivetrain::getPulseSpeed(void)
{
    return pulseSpeed;
}

void Drivetrain::reverseOrientation(void)
{
    debugPrint("Reversing Orientation of Robot\n");
    drivingSpeed = drivingSpeed * -1.0;
    pulseSpeed = drivingSpeed * -1.0;
    debugPrint("Driving Speed: ");
    debugPrint(drivingSpeed, true);
    orientation = drivingSpeed < 0 ? Backwards : Forwards;
}

Drivetrain::ForBack Drivetrain::getOrientation(void)
{
    debugPrint("Orientation is: ");
    debugPrint(orientation == Forwards ? "Forwards\n" : "Backwards\n");
    return orientation;
}

float Drivetrain::getHeading(void)
{
    int count = 0;
    float heading = RPS.Heading();
    while (heading == -1.0 && count < 100)
    {
        heading = RPS.Heading();
        count++;
        LCD.Write("Heading is wrong: ");
        LCD.WriteLine(count);
        Sleep(250);
    }
    switch (getOrientation())
    {
        case Backwards:
            return heading;
        case Forwards:
            float newHeading = heading + 180.0;
            newHeading = newHeading >= 360.0 ? newHeading - 360.0 : newHeading;
            debugPrint("Corrected Heading: ");
            debugPrint(newHeading);
            return newHeading;
    }
}

void Drivetrain::setCorrect(void)
{
    LCD.Clear();
    LCD.WriteLine("Set at Salt\nPress Middle");
    while (!middlePressed())
    {
        LCD.Clear();
        LCD.Write("Heading: ");
        LCD.WriteLine(RPS.Heading());
        LCD.Write("X: ");
        LCD.WriteLine(RPS.X());
        LCD.Write("Y: ");
        LCD.WriteLine(RPS.Y());
        LCD.Write("\nOriginal X: ");
        LCD.WriteLine(SALT_X);
        LCD.Write("Original Y: ");
        LCD.WriteLine(SALT_Y);
        Sleep(300);
    }
    corrX = RPS.X() - SALT_X ;
    corrY = RPS.Y() - SALT_Y ;
    corrHead = RPS.Heading() - 315.0;
}

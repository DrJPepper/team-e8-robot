#ifndef TESTS_H
#define TESTS_H

#include <FEHLCD.h>
#include <FEHUtility.h>
#include <FEHBattery.h>
#include <cstring>
#include <globals.h>
#include <drivetrain.h>
#include <colorsensor.h>
#include <buttons.h>
#include <salt.h>
#include <crank.h>
#include <switch.h>

class Tests
{
    private:
        Drivetrain& dT;
        ColorSensor& cS;
        Buttons& bT;
        Salt& nA;
        Crank& cR;
        Switch& sW;
        // DT
        void testEncAndRPS(void);
        void testRotationAndRPS(void);
        void printRPS(void);
        void testMotors(void);
        void testEnc(void);
        void testLinearCorrect(void);
        void testAngularCorrect(void);
        void testPulse(void);
        // Color
        void testColors(void);
        // Buttons
        void calibrateServo(void);
        void printAngle(void);
        void runShit(void);
        // PT's
        void pT2(void);
        void pT3(void);
        void pT4(void);
        void pT5(void);
        // Battery
        void printBattery(void);
        // Salt
        void calibrateSalt(void);
        void printSaltAngle(void);
        // Crank
        void runCrank(void);
    public:
        Tests(Drivetrain& drive, ColorSensor& color, Buttons& buttons, Salt& salt, Crank& crank, Switch& switchNotKeyword) :
            dT(drive), cS(color), bT(buttons), nA(salt), cR(crank), sW(switchNotKeyword){}
        void testMenu(void);

};

#endif // TESTS_H

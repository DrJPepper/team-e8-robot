#include <globals.h>

ButtonBoard b(FEHIO::Bank3);
bool debug = false;
ButtonBoard &buttons(void)
{
    return b;
}

float distance(float x1, float y1, float x2, float y2)
{
    return sqrt(pow((y2 - y1), 2.0) + pow((x2 - x1), 2.0));
}

void waitForMiddlePress(void)
{
    // In case it's pressed at start
    if (buttons().MiddlePressed())
    {
        while (buttons().MiddlePressed());
    }
    while (!buttons().MiddlePressed());
    while (buttons().MiddlePressed());
}

bool middlePressed(void)
{
    buttons().MiddlePressed();
}

bool rightPressed(void)
{
    buttons().RightPressed();
}

bool leftPressed(void)
{
    buttons().LeftPressed();
}

int getPressed(void)
{
    while (true)
    {
        if (buttons().LeftPressed())
        {
            while(buttons().LeftPressed());
            return 0;
        }
        if (buttons().MiddlePressed())
        {
            while(buttons().MiddlePressed());
            return 1;
        }
        if (buttons().RightPressed())
        {
            while(buttons().RightPressed());
            return 2;
        }
    }
}

void debugPrint(char const *str)
{
    if (debug) LCD.Write(str);
}

void debugPrint(char const *str, bool sleep)
{
    debugPrint(str);
    if (sleep && debug) waitForMiddlePress();
}

void debugPrint(float num)
{
    if (debug) LCD.WriteLine(num);
}

void debugPrint(float num, bool sleep)
{
    debugPrint(num);
    if (sleep && debug) waitForMiddlePress();
}

void debugPrint(int i)
{
    if (debug) LCD.WriteLine(i);
}

void debugPrint(int i, bool sleep)
{
    debugPrint(i);
    if (sleep && debug) waitForMiddlePress();
}

void debugPrint(bool b)
{
    if (debug) LCD.WriteLine(b);
}

void debugPrint(bool b, bool sleep)
{
    debugPrint(b);
    if (sleep && debug) waitForMiddlePress();
}

void debugClear(void)
{
    if (debug) LCD.Clear();
}

void toggleDebug(void)
{
    debug = !debug;
}

bool getDebug(void)
{
    return debug;
}

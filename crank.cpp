#include "crank.h"

void Crank::run(void)
{
    dT.driveTo(29.7, 46.8, 0.2, true, 90);
    dT.driveTo(29.7, 51.4, 0.2, true, 90); // Up to crank
    dT.rotate(95.5);
    ColorSensor::Color color = cS.getColor();
    //debugPrint(color == cS.NoColor ? "No Color" : "Color");
    float startTime = TimeNowMSec();
    while((color == cS.NoColor) && (TimeNowMSec() - 4000 < startTime))
    {
        dT.drive(0.2);
        color = cS.getColor();
    }
    dT.drive(0.17);
    Sleep(500);
    color = cS.getColor();
    if (color == cS.Red)
    {
        debugPrint("Red\n");
        motor.SetPercent(-CRANK_MOTOR_SPEED);
        Sleep(CRANK_RUN_TIME + 95);
    }
    else if (color == cS.Blue)
    {
        debugPrint("Blue\n");
        motor.SetPercent(CRANK_MOTOR_SPEED);
        Sleep(CRANK_RUN_TIME);
    }
    else
    {
        LCD.WriteLine("No color for crank");
        motor.SetPercent(-CRANK_MOTOR_SPEED);
        Sleep(CRANK_RUN_TIME + 95);
    }
    dT.drive(0.0);
    motor.SetPercent(0.0);
    debugPrint("Done with crank\n", true);
    dT.drive(-dT.getDrivingSpeed());
    Sleep(500);
    dT.drive(0.0);
}

void Crank::runMotor(Direction dir)
{
    if (dir == Right)
        motor.SetPercent(CRANK_MOTOR_SPEED);
    else
        motor.SetPercent(-CRANK_MOTOR_SPEED);
}

void Crank::stopMotor(void)
{
    motor.SetPercent(0.0);
}

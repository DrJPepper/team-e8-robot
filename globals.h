#ifndef GLOBALS_H
#define GLOBALS_H

#include <FEHIO.h>
#include <FEHUtility.h>
#include <FEHLCD.h>
#include <math.h>

#define PI 3.14159265

// DRIVETRAIN
// Pins
#define RIGHT_MOTOR FEHMotor::Motor1
#define LEFT_MOTOR FEHMotor::Motor0
#define RIGHT_ENCODER FEHIO::P0_0
#define LEFT_ENCODER FEHIO::P0_1
// Constants
// Speed to use when correcting
#define PULSE_SPEED float(0.8) // Was .75
#define PULSE_DELAY 45 // Was 40
// Encoder constants
#define CLICKS_PER_DEGREE float(2.7) // Was 2.8
#define CLICKS_PER_INCH 35 // Was 36
// Speed constants
#define TURNING_SPEED 0.35 // Was .29
#define DRIVING_SPEED 0.30 // Was .23
#define INSTRUCTION_DELAY 100
// RPS constants
#define LINEAR_DEADBAND float(0.4)
#define ANGULAR_DEADBAND float(0.6) // Was 0.4
// Special Point
#define SALT_X 22.8
#define SALT_Y 13.4

// COLOR SENSOR
#define CDS_1 FEHIO::P0_2
#define CDS_2 FEHIO::P0_3
// Do not make calls to these directly, use
// getter from class
#define BLUE_MAX float(1.2)
#define BLUE_MIN float(0.4)
#define RED_MAX float(0.25)
#define RED_MIN float(0.0)

// BUTTONS
#define BUTTONS_SERVO FEHServo::Servo0
#define RED_POS 55
#define BLUE_POS 11
#define WHITE_POS 31
#define SERVO_MAX 2386
#define SERVO_MIN 500
#define UP_ANGLE 99 // Was 102

// Crank
#define CRANK_RUN_TIME 1825 // Depends on 40%
#define CRANK_MOTOR FEHMotor::Motor3
#define CRANK_MOTOR_SPEED 40 // PERCENT

// Salt
#define SALT_SERVO FEHServo::Servo7
#define SALT_SWITCH FEHIO::P2_0
#define SALT_DOWN 0
#define SALT_UP 120
#define SALT_RETRACTED 162
#define SALT_MAX 2051
#define SALT_MIN 500

// THINGS THAT MIGHT AS WELL BE GLOBAL
enum Direction {Right, Left};
typedef AnalogInputPin CdS;
ButtonBoard &buttons(void);
void waitForMiddlePress(void);
void debugPrint(char const *);
void debugPrint(float);
void debugPrint(bool);
void debugPrint(int);
void debugPrint(char const *, bool);
void debugPrint(float, bool);
void debugPrint(bool, bool);
void debugPrint(int, bool);
void debugClear(void);
void toggleDebug(void);
bool getDebug(void);
int getPressed(void);
float distance(float, float, float, float);
bool leftPressed(void);
bool middlePressed(void);
bool rightPressed(void);

#endif // GLOBALS_H

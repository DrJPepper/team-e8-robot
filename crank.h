#ifndef CRANK_H
#define CRANK_H

#include <FEHMotor.h>
#include <FEHUtility.h>
#include <globals.h>
#include <colorsensor.h>
#include <drivetrain.h>

class Crank
{
private:
    ColorSensor& cS;
    Drivetrain& dT;
    FEHMotor motor;
public:
    Crank(ColorSensor& color, Drivetrain& drive): dT(drive), motor(CRANK_MOTOR), cS(color){}
    void run(void);
    void runMotor(Direction); // For testing purposes
    void stopMotor(void); // For testing purposes
};

#endif // CRANK_H

#ifndef BUTTONS_H
#define BUTTONS_H

#include <FEHServo.h>
#include <FEHRPS.h>
#include <FEHUtility.h>
#include <globals.h>
#include <drivetrain.h>

class Buttons
{
    public:
        // Make private later
        enum Color {Red, White, Blue};
        void moveTo(Color);
    private:
        FEHServo servo;
        Drivetrain& dT;
        bool isPressed(Color);
    public:
        Buttons(Drivetrain&);
        void calibrate(void);
        void run(void);
        void setAngle(int);
};

#endif // BUTTONS_H

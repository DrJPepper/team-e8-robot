#include <tests.h>

void Tests::pT2(void)
{
    //cS.waitForStart();
    dT.driveTo(17.9, 22.1);
    dT.driveTo(29.5, 22.1);
    dT.driveTo(29.5, 56.6, -float(0.35));
}

void Tests::pT3(void)
{
    LCD.Clear();
    //cS.waitForStart();
    dT.reverseOrientation();
    dT.driveTo(17.9, 22.1);
    dT.driveTo(29.5, 22.1);
    dT.driveToWithSpeed(28.0, 48.8, 0.35);
    dT.driveTo(21.1, 48.1);
    dT.driveTo(16.2, 59.5);
    //dT.driveTo(31.3, 48.6, false);
    //dT.driveTo(15.5, 58.8, false);
    //bT.run();
}

void Tests::pT4(void)
{
    /*//dT.setSalt();
    LCD.Clear();
    //cS.waitForStart();
    nA.raise();
    dT.driveTo(18.4, 19.3);
    dT.driveToWithHeading(dT.getSaltX(), dT.getSaltY(), 315.0);
    nA.lower();
    dT.drive(-DRIVING_SPEED);
    Sleep(1500);
    dT.drive(0.0);
    Sleep(500);
    dT.drive(DRIVING_SPEED);
    Sleep(700);
    dT.drive(0.0);
    Sleep(500);
    nA.raise();
    dT.reverseOrientation();
    dT.driveTo(22.6, 16.5);
    dT.driveTo(28.2, 24.5);
    dT.driveToWithSpeed(30.1, 48.4, 0.6);
    dT.driveTo(29.7, 48.3);
    dT.rotate(135.0-RPS.Heading());
    dT.driveTo(26.2,51.8);
    nA.lower();
    dT.rotate(200.0-RPS.Heading());
    dT.rotate(150.0-RPS.Heading());
    dT.driveTo(15.5,57.7);
    dT.rotate(170.0-RPS.Heading());
    dT.driveTo(10.0,58.6);
    while (!middlePressed())
    {
        dT.drive(0.0);
        Sleep(300);
    }*/
}

void Tests::pT5(void)
{
    nA.retract(false);
    cS.waitForStart();
    dT.driveTo(17.9, 19.4);
    dT.reverseOrientation();
    dT.driveTo(27.8, 20.5, 100.0);
    dT.driveToWithSpeed(29.6, 41.8, 0.5);
    dT.driveToWithHeading(30.0, 49.2, 90.0);
    dT.driveToWithHeading(30.0, 54.0, 90.0);
    cR.run();
    dT.reverseOrientation();
    dT.driveTo(28.3, 20.6);
    dT.reverseOrientation();
    dT.driveToWithHeading(10.7, 23.2, 273.0);
    bT.setAngle(50);
    Sleep(500);
    dT.drive(0.3);
    waitForMiddlePress();
}

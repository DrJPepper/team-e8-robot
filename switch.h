#ifndef SWITCH_H
#define SWITCH_H

#include <drivetrain.h>
#include <globals.h>
#include <FEHUtility.h>
#include <buttons.h>

class Switch
{
private:
    Drivetrain& dT;
    Buttons& bT;
public:
    Switch(Drivetrain& drive, Buttons& buttons): dT(drive), bT(buttons){}
    void run(void);
};

#endif // SWITCH_H

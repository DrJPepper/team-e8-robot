#include <FEHLCD.h>
#include <FEHUtility.h>
#include <FEHIO.h>
#include <FEHRPS.h>
#include <FEHMotor.h>
#include <math.h>
#include <globals.h>

#ifndef DRIVETRAIN_H
#define DRIVETRAIN_H

using namespace std;

class Drivetrain
{
    public:
        enum ForBack {Forwards, Backwards};
    private:
        // OBJECTS
        FEHMotor right_motor;
        FEHMotor left_motor;
        DigitalEncoder right_encoder;
        DigitalEncoder left_encoder;
        // Only used in one method, might remove
        // VARS
        float drivingSpeed;
        float turningSpeed;
        ForBack orientation;
        float pulseSpeed;
        float corrY;
        float corrX;
        float corrHead;
        // MEHTODS
        // Used to pulse for corrections
        void drive(float, bool);
        void drive(ForBack, bool);
        float getTurningSpeed(void);
        float getHeading(void);
        float calcAngularError(float);
    public:
        // CONSTRUCTOR
        Drivetrain(void): right_motor(RIGHT_MOTOR), left_motor(LEFT_MOTOR),
            right_encoder(RIGHT_ENCODER), left_encoder(LEFT_ENCODER), drivingSpeed(-DRIVING_SPEED),
            turningSpeed(-TURNING_SPEED), pulseSpeed(-PULSE_SPEED), corrX(0.0), corrY(0.0), corrHead(0.0),
            orientation(Backwards){}
        // METHODS
        void resetEncoders(void);
        // Just sets motors
        void drive(float);
        void turn(Direction, float);
        void turn(Direction, float, bool);
        // Uses RPS and encoders
        void rotate(float, bool);
        void rotate(float);
        void driveTo(float, float, float, bool, float);
        void driveTo(float, float);
        void driveTo(float, float, bool);
        void driveToWithHeading(float, float, float);
        void driveToWithHeading(float, float, float, bool);
        void driveToWithSpeed(float, float, float);
        void driveToWithSpeed(float, float, float, bool);
        // PRINTS VALUES
        int clicks(Direction);
        int clicks(void);
        void reverseOrientation(void);
        ForBack getOrientation(void);
        float getDrivingSpeed(void);
        float getPulseSpeed(void);
        // Should be private after it's working
        void turn(Direction, bool);
        void setCorrect(void);
};

#endif // DRIVETRAIN_H

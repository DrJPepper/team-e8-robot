#include "salt.h"

Salt::Salt(Drivetrain& drivetrain): servo(SALT_SERVO), dT(drivetrain), sSwitch(SALT_SWITCH), in(false)
{
    servo.SetMax(SALT_MAX);
    servo.SetMin(SALT_MIN);
}

void Salt::raise(void)
{
    raise(true);
}

void Salt::raise(bool pause)
{
    servo.SetDegree(SALT_UP);
    float startTime = TimeNowMSec();
    while (pause && (TimeNowMSec() - 1000 < startTime))
    {
        if (getSwitch()) in = true;
    }
}

void Salt::lower(void)
{
    servo.SetDegree(SALT_DOWN);
    Sleep(500);
}

void Salt::retract(void)
{
    servo.SetDegree(SALT_RETRACTED);
    Sleep(500);
}

void Salt::retract(bool pause)
{
    servo.SetDegree(SALT_RETRACTED);
    Sleep(pause ? 500 : 0);
}

void Salt::setAngle(int angle)
{
    servo.SetDegree(angle);
}

void Salt::calibrate(void)
{
    servo.Calibrate();
    Sleep(200);
    waitForMiddlePress();
}

void Salt::run(void)
{
    lower(); // Get Salt
    dT.drive(-0.2);
    Sleep(2000);
    dT.drive(0.0);
    Sleep(100);
    dT.drive(0.2);
    Sleep(1000);
    dT.drive(0.0);
    Sleep(100);
    raise();
    if (RPS.Heading() == -1.0)
    {
        lower();
        raise();
    }
}

void Salt::deposit(void)
{
    dT.reverseOrientation();
    dT.driveTo(10.0, 50.9, 135.0);
    dT.reverseOrientation();
    dT.driveTo(11.3, 50.6);
    dT.rotate(315.0);
    lower();
    dT.reverseOrientation();
    dT.driveTo(6.8, 56.1);
    for (int i = 0; i < 4; i++)
    {
        dT.turn(Right, 0.75, false);
        Sleep(500);
        dT.turn(Left, 0.75, false);
        Sleep(500);
    }
    dT.drive(0.0);
    dT.reverseOrientation();
    dT.turn(Right, TURNING_SPEED, false);
    Sleep(500);
    dT.drive(dT.getDrivingSpeed());
    Sleep(250);
    dT.driveTo(9.4, 50.5);
    raise();
}

bool Salt::getSwitch(void)
{
    return !sSwitch.Value();
}

bool Salt::bagIn(void)
{
    return in;
}

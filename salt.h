#ifndef SALT_H
#define SALT_H

#include <FEHUtility.h>
#include <FEHMotor.h>
#include <FEHIO.h>
#include <FEHServo.h>
#include <globals.h>
#include <drivetrain.h>

class Salt
{
private:
    FEHServo servo;
    DigitalInputPin sSwitch;
    Drivetrain& dT;
    bool in;
public:
    Salt(Drivetrain&);
    void setAngle(int);
    void retract(void);
    void retract(bool);
    void raise(void);
    void raise(bool);
    void lower(void);
    void calibrate(void);
    void run(void);
    void deposit(void);
    bool getSwitch(void);
    bool bagIn(void);
};

#endif // SALT_H

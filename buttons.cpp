#include "buttons.h"

Buttons::Buttons(Drivetrain& drivetrain): servo(BUTTONS_SERVO), dT(drivetrain)
{
    servo.SetMax(SERVO_MAX);
    servo.SetMin(SERVO_MIN);
    servo.SetDegree(UP_ANGLE);
}

void Buttons::calibrate(void)
{
    servo.Calibrate();
    Sleep(200);
    waitForMiddlePress();
}

void Buttons::setAngle(int angle)
{
    servo.SetDegree(angle);
}

void Buttons::moveTo(Color color)
{
    switch (color)
    {
    case Red:
        setAngle(RED_POS);
        break;
    case White:
        setAngle(WHITE_POS);
        break;
    case Blue:
        setAngle(BLUE_POS);
        break;
    }
}

bool Buttons::isPressed(Color color)
{
    switch (color)
    {
    case Red:
        return RPS.RedButtonPressed();
    case White:
        return RPS.WhiteButtonPressed();
    case Blue:
        return RPS.BlueButtonPressed();
    }
}

void Buttons::run(void)
{
    debugPrint("Lining up\n");
    dT.driveToWithHeading(27.2, 52.8, 135); // Try to avoid snow
    dT.driveTo(22.6, 57.8);
    dT.driveToWithHeading(16.5, 57.0, 135.0); // Line up
    Color colors[3];
    colors[RPS.BlueButtonOrder() - 1] = Blue;
    colors[RPS.RedButtonOrder() - 1] = Red;
    colors[RPS.WhiteButtonOrder() - 1] = White;
    for (int i = 0; i < 3; i++)
    {
        int attempt = 0;
        while(!isPressed(colors[i]) && attempt < 3)
        {
            debugPrint("On: ");
            debugPrint(colors[i] == Red ? "Red" : colors[i] == Blue ? "Blue" : "White" "\n");
            moveTo(colors[i]);
            debugPrint("Set Angle\n");
            Sleep(500);
            dT.drive(dT.getDrivingSpeed() - 0.06);
            debugPrint("Forward\n", true);
            float startTime = TimeNowMSec();
            while(!isPressed(colors[i]) && TimeNowMSec() - 2500 < startTime);
            setAngle(UP_ANGLE);
            debugPrint("Backwards\n", true);
            dT.reverseOrientation();
            dT.driveToWithHeading(16.5, 57.0, 315);
            dT.reverseOrientation();
            debugPrint("Completed Press\n", true);
            attempt++;
        }
    }
    setAngle(UP_ANGLE);
}
